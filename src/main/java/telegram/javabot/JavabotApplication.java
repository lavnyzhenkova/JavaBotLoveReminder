package telegram.javabot;

import telegram.javabot.service.SQL;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;


@SpringBootApplication
public class JavabotApplication {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        SpringApplication.run(JavabotApplication.class, args);
        System.out.println("Hello world!");
        SQL sql = new SQL();
        sql.sqlmethod();
    }
}
