package telegram.javabot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@Table(name = "amour")
@Entity
public class Amour {
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Lob
    @Column(name = "Mess", nullable = false)
    private String mess;

    static final String DB_URL = "jdbc:mysql://localhost:3306/bot";
    static final String DB_USER = "jav";
    static final String DB_PASSWD = "Password_2";

    public static String getMess() {
        Connection conn = null;
        Statement st;
        String messAmour = null;
        try {
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWD);

            st = conn.createStatement();

            String SQL1 = "select Mess from Amour order by rand() limit 1;";
            ResultSet mess = st.executeQuery(SQL1);

            while (mess.next()) {
                messAmour = mess.getString(1);
                System.out.println(messAmour);
            }
            mess.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messAmour;
    }
    public void setMess(String mess) {
        this.mess = mess;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}