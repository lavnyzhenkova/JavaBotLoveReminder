package telegram.javabot.service;

import telegram.javabot.Amour;
import telegram.javabot.Mood;
import telegram.javabot.Quarrel;
import telegram.javabot.Suport;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class TelegramBot extends TelegramLongPollingBot {
    @Override
    public String getBotUsername() {
        return "reminder_oflove_bot";
    }

    @Override
    public String getBotToken() {
        return "2094946645:AAGCuhV5NniAMdVqkArrtKWPBzlrJ0jQeBo";
    }

    @Override
    public void onUpdateReceived(Update update) {
        SQL sqlText = new SQL();
        Mood mood = new Mood();
        Amour amour = new Amour();
        Quarrel quarrel = new Quarrel();
        Suport suport = new Suport();

        Message message = update.getMessage();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("да, малыш, я тебя услышал(-а), ты сказал(-а): " + message.getText());
        sendMessage.setChatId(String.valueOf(message.getChatId()));

        if (message.getText().equals("/start")) {
            String text = "привет! здесь всё сделано с любовью\n";

            sendMessage.enableMarkdown(true);
            ReplyKeyboardMarkup keyboardMarkup = getLoveKeybord();
            sendMessage.setReplyMarkup(keyboardMarkup);
            sendMessage.setText(text);
        }

        if (message.getText().equals("мне грустно") || message.getText().equals("мне плохо")) {
            String text = Mood.getMess();
            sendMessage.setText(text);
        }
        if (message.getText().equals("хочу любви") || message.getText().equals("прояви любовь")) {
            String text = Amour.getMess();
            sendMessage.setText(text);
        }
        if (message.getText().equals("мне нужна твоя поддержка") || message.getText().equals("нужна поддержка") || message.getText().equals("поддержи меня")) {
            String text = Suport.getMess();
            sendMessage.setText(text);
        }

        if (message.getText().equals("мы в ссоре") || message.getText().equals("ссора") || message.getText().equals("мне обидно") || message.getText().equals("ты меня обидел")) {
            String text = Quarrel.getMess();
            sendMessage.setText(text);
        }

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private ReplyKeyboardMarkup getLoveKeybord() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboardRows = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add("мне грустно");
        keyboardRow.add("хочу любви");
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardRow.add("поддержи меня");
        keyboardRow.add("мы в ссоре");
        keyboardRows.add(keyboardRow);
        keyboardRows.add(keyboardSecondRow);
        replyKeyboardMarkup.setKeyboard(keyboardRows);
        return replyKeyboardMarkup;

    }

}
