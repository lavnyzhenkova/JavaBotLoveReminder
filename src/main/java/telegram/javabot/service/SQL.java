package telegram.javabot.service;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;

public class SQL {

    public String sqlmethod() throws SQLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        final String DB_URL = "jdbc:mysql://localhost:3306/bot";
        final String DB_USER = "jav";
        final String DB_PASSWD = "Password_2";

        Connection conn = null;
        Statement st;
        try {
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWD);
            System.out.println("Connected!");

            st = conn.createStatement();

            String SQL1 = "select Mess from Mood order by rand() limit 1;";
            ResultSet rs1 = st.executeQuery(SQL1);

            while (rs1.next()) {

                String messMood = rs1.getString(1);
                System.out.println(messMood);

            }
            rs1.close();

            String SQL2 = "select Mess from Amour order by rand() limit 1;";
            ResultSet rs2 = st.executeQuery(SQL2);

            while (rs2.next()) {
                String messAmour = rs2.getString(1);
                System.out.println(messAmour);
            }
            rs2.close();

            String SQL3 = "select Mess from Quarrel order by rand() limit 1;";
            ResultSet rs3 = st.executeQuery(SQL3);

            while (rs3.next()) {
                String messQuarrel = rs3.getString(1);
                System.out.println(messQuarrel);
            }
            rs3.close();

            String SQL4 = "select Mess from Suport order by rand() limit 1;";
            ResultSet rs4 = st.executeQuery(SQL4);

            while (rs4.next()) {
                String messSuport = rs4.getString(1);
                System.out.println(messSuport);
            }
            rs4.close();

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DB_URL;
    }
}
